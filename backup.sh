#!/bin/bash
screen -r mcs -X stuff '/save-all\n/save-off\n'
/usr/local/bin/gsutil cp -R ${BASH_SOURCE%/*}/world gs://voziv-game-servers-minecraft-backups/bugfyn-$(date "+%Y%m%d-%H%M%S")-world
screen -r mcs -X stuff '/save-on\n'
